package cl.jarapulgar.Main;

import cl.jarapulgar.Algorithm.Approximation2;
import cl.jarapulgar.Algorithm.Approximation2Better;
import cl.jarapulgar.Algorithm.Approximationp;
import cl.jarapulgar.Graph.Graph;
import cl.jarapulgar.Graph.GraphGenerator;

import java.io.IOException;
import java.util.Arrays;

import static cl.jarapulgar.Graph.GraphGenerator.getGraphOfText;

/**
 * Created by nicol on 29-06-2017.
 */
public class Main {
    public static void main(String[] args) throws IOException {

        performance();
        for (int i = 10; i <= 20; i++) {
            System.out.println("==============================================================");
            int vertices = (int) Math.pow(2, i);
            System.out.println("N° Vértices: " + vertices);

            long totalTime = System.nanoTime();
            Graph graph1 = getGraphOfText("externo1.pg", vertices);
            long endTime = System.nanoTime() - totalTime;
            System.out.println("Tiempo crear Grafo: " + endTime);
            totalTime = System.nanoTime();
            //   graph1.printMatriz();
            Approximation2 approximation2 = new Approximation2();
            approximation2.start(graph1, graph1.vertices);
            endTime = System.nanoTime() - totalTime;
            System.out.println("Tiempo 2-Aproximacion: " + endTime);
            //    System.out.println(Arrays.toString(approximation2.getArrayVertices().toArray()));

            totalTime = System.nanoTime();
            Approximationp approximationp = new Approximationp();
            graph1.setGrado();
            approximationp.start(graph1, graph1.vertices);
            endTime = System.nanoTime() - totalTime;
            System.out.println("Tiempo p-Aproximacion: " + endTime);
            //    System.out.println(Arrays.toString(approximation2.getArrayVertices().toArray()));

            totalTime = System.nanoTime();
            //  graph1.setGrado();
            Approximation2Better approximation2Better = new Approximation2Better();
            approximation2Better.start(graph1, graph1.vertices);
            endTime = System.nanoTime() - totalTime;
            System.out.println("Tiempo 2-Aproximacion Mejorada: " + endTime);
            //    System.out.println(Arrays.toString(approximation2.getArrayVertices().toArray()));
        }
    }

    public static void performance() {
        System.out.println("==============================================================");
        System.out.println("Available processors (cores): " + Runtime.getRuntime().availableProcessors());
        System.out.println("Free memory (available to the JVM) (MB): " + Runtime.getRuntime().freeMemory() / (1024.0 * 1024.0));
        long maxMemory = Runtime.getRuntime().maxMemory();
        System.out.println("Maximum memory (MB): " + (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemory / (1024.0 * 1024.0)));
        System.out.println("Total memory currently in use by the JVM (MB): " + Runtime.getRuntime().totalMemory() / (1024.0 * 1024.0));
        System.out.println("==============================================================");
    }
}

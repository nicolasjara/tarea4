package cl.jarapulgar.Algorithm;

import cl.jarapulgar.Graph.Graph;

import java.util.ArrayList;

/**
 * Created by nicol on 29-06-2017.
 */
public class Approximation2 {
    ArrayList<Integer> arrayVertices;
    ArrayList<Integer> arrayArista;
    int vertices;

    public void start(Graph graph, int vertices) {
        this.vertices = vertices;
        arrayVertices = new ArrayList<>();
        arrayArista = new ArrayList<>();

        while (true) {
            int[] posiciones = getArista(graph.matriz);
            if (posiciones[0] != -1 && posiciones[1]!=-1) {
                arrayVertices.add(posiciones[0]);
                arrayVertices.add(posiciones[1]);
                for (int i = posiciones[0]; i < vertices; i++) {
                    graph.deleteArista(i, posiciones[1]);
                }
                for (int j = posiciones[1]; j < vertices; j++) {
                    graph.deleteArista(posiciones[0], j);
                }
             //   graph.printMatriz();
            }else {
                break;
            }
        }
    }

    public int[] getArista(int[][] matriz) {
        for (int i = 0; i < vertices; i++) {
            for (int j = 0; j < vertices; j++) {
                if (matriz[i][j] > 0) {
                 //   System.out.println(matriz[i][j]);
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{-1, -1};
    }

    public ArrayList<Integer> getArrayVertices() {
        return arrayVertices;
    }
}

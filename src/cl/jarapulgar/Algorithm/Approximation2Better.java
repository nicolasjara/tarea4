package cl.jarapulgar.Algorithm;

import cl.jarapulgar.Graph.Graph;

import java.util.ArrayList;

/**
 * Created by nicol on 07-07-2017.
 */
public class Approximation2Better {
    ArrayList<Integer> arrayVertices;
    ArrayList<Integer> arrayArista;
    int vertices;

    public void start(Graph graph, int vertices) {
        this.vertices = vertices;
        arrayVertices = new ArrayList<>();
        arrayArista = new ArrayList<>();
        while (true) {
            int valores[] = graph.getAristaAdyacente();
            if (valores[1] != -1) {
                arrayVertices.add(valores[0]);
                arrayVertices.add(valores[1]);
                for (int i = valores[0]; i < vertices; i++) {
                    graph.deleteArista(i, valores[1]);
                }
                for (int j = valores[1]; j < vertices; j++) {
                    graph.deleteArista(valores[0], j);
                }
                //   graph.printMatriz();
            } else {
                //   System.out.println("termino_________termino_________termino_________termino_________");
                break;
            }
        }
    }


    public int[] getArista(int[][] matriz) {
        for (int i = 0; i < vertices; i++) {
            for (int j = 0; j < vertices; j++) {
                if (matriz[i][j] > 0) {
                    //   System.out.println(matriz[i][j]);
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{-1, -1};
    }

    public ArrayList<Integer> getArrayVertices() {
        return arrayVertices;
    }
}

package cl.jarapulgar.Algorithm;

import cl.jarapulgar.Graph.Graph;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by nicol on 07-07-2017.
 */
public class Approximationp {
    ArrayList<Integer> arrayVertices;
    ArrayList<Integer> arrayArista;
    int vertices;

    public void start(Graph graph, int vertices) {
        this.vertices = vertices;
        arrayVertices = new ArrayList<>();
        arrayArista = new ArrayList<>();
        while (true) {
            int grado = graph.getmaxGrado();
            if (grado != 0) {
                arrayVertices.add(grado);

                for (int i = 0; i < vertices; i++) {
                    graph.deleteAristaByGrade(i, grado);
                }
                //   graph.printMatriz();
            } else {
             //   System.out.println("termino_________termino_________termino_________termino_________");
                break;
            }
        }
    }


    public int[] getArista(int[][] matriz) {
        for (int i = 0; i < vertices; i++) {
            for (int j = 0; j < vertices; j++) {
                if (matriz[i][j] > 0) {
                    //   System.out.println(matriz[i][j]);
                    return new int[]{i, j};
                }
            }
        }
        return new int[]{-1, -1};
    }

    public ArrayList<Integer> getArrayVertices() {
        return arrayVertices;
    }
}

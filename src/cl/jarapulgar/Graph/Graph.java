package cl.jarapulgar.Graph;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by nicol on 29-06-2017.
 */
public class Graph implements Serializable {

    public int[][] matriz;
    public int vertices;
    public int aristas;
    public HashMap<Integer, Integer> arrayVerticesGrado;


    public Graph(int vertices, int aristas) {
        this.matriz = new int[vertices][vertices];
        this.vertices = vertices;
        this.aristas = aristas;
        arrayVerticesGrado = new HashMap<>();
    }

    public void init() {
        for (int i = 0; i < vertices; i++) {
            for (int j = 0; j < vertices; j++) {
                matriz[i][j] = -1;
            }
        }
    }

    public int getmaxGrado() {
        int max = 0;

        for (int i = 0; i < arrayVerticesGrado.size(); i++) {
            // System.out.println(arrayVerticesGrado.size() + "  " + i);
            try {
                int current = arrayVerticesGrado.get(i);
                if (current == 1) {
                    max = i;
                }
            } catch (Exception e) {
                //     System.out.println("valor: " + i);
                //  e.printStackTrace();
            }
        }
        if (max > 0) {
            arrayVerticesGrado.remove(max);
        }
        return max;
    }

    public int[] getAristaAdyacente() {
        int[] arista = new int[2];
        arista[0] = -1;
        arista[1] = -1;
        int max = getmaxGrado();
        for (int k = 0; k < vertices; k++) {
            if (matriz[max][k] == 1) {
                arista[1] = k;
            }
        }
        arista[0] = max;
        return arista;
    }

    public void setGrado() {
        for (int i = 0; i < vertices; i++) {
            int contador = 0;
            for (int j = 0; j < vertices; j++) {
                if (matriz[i][j] == 1) {
                    contador++;
                }
            }
            arrayVerticesGrado.put(i, contador);
        }
    }

    public void setArista(int i, int j) {
        matriz[i][j] = 1;
        matriz[j][i] = 1;
    }

    public void deleteArista(int i, int j) {
        matriz[i][j] = -1;
        matriz[j][i] = -1;
    }

    public void deleteAristaByGrade(int i, int j) {
        matriz[i][j] = -1;
    }

    public void printMatriz() {
        StringBuilder stringBuilderHead = new StringBuilder();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilderHead.append("  ");
        for (int i = 0; i < vertices; i++) {
            stringBuilderHead.append("  ");
            stringBuilderHead.append(i);
            stringBuilder.append(i);
            stringBuilder.append(" [");
            for (int j = 0; j < vertices; j++) {
                stringBuilder.append(" ");
                stringBuilder.append(matriz[i][j]);
                stringBuilder.append(" ");
            }
            stringBuilder.append("]\n");
        }
        System.out.println(stringBuilderHead.toString());
        System.out.println(stringBuilder.toString());
    }
}

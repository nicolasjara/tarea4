package cl.jarapulgar.Graph;

import java.io.*;

/**
 * Created by nicol on 29-06-2017.
 */
public class GraphGenerator {

    public static Graph getGraphOfText(String file) throws IOException {
        FileInputStream fstream = new FileInputStream(file);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        Graph graph = new Graph(Integer.parseInt(br.readLine()), Integer.parseInt(br.readLine()));
        while ((strLine = br.readLine()) != null) {
            String[] resultString = strLine.split(" ");
            graph.setArista(Integer.parseInt(resultString[0]), Integer.parseInt(resultString[1]));
        }
        in.close();
        return graph;
    }

    public static Graph getGraphOfText(String file, int max) throws IOException {
        FileInputStream fstream = new FileInputStream(file);
        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        int vertices = Integer.parseInt(br.readLine());
        vertices = vertices < max ? vertices : max;
        Graph graph = new Graph(vertices, Integer.parseInt(br.readLine()));
        while ((strLine = br.readLine()) != null) {
            String[] resultString = strLine.split(" ");
            if (Integer.parseInt(resultString[0]) < max && Integer.parseInt(resultString[1]) < max) {
               // System.out.println(resultString[0]+"   "+resultString[1]);
                graph.setArista(Integer.parseInt(resultString[0]), Integer.parseInt(resultString[1]));
            }
        }
        in.close();
        return graph;
    }
}

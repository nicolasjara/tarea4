El main del proyecto se encuentra en la carpeta src/cl/jarapulgar/Main/Main.java

Para correr solo se necesita compilarlo, ya que, existe un ciclo en el cual automaticamente se setea automaticamente la cantidad de vertices, para N = 2^i, i={10,11,...,20}.

Al ejecutar el Main, este arrojará por la salida estandar los prints correspondientes a los tiempos en nanosegundos de creación del grafo y el tiempo que toma cada algoritmo en encontrar el conjunto cobertor del grafo.

* (La tarea fue desarrollada en IntelliJ y fue probada tambien en Eclipse, pero no trae el workspace de eclipse)
